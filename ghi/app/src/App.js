import Nav from "./Nav";
import MainPage from "./MainPage";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeeForm from "./AttendeeForm";
import PresentationForm from "./PresentationForm";
import { BrowserRouter as Router, Routes, Route, Outlet} from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }

  return (
    <Router>
      <>
        <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendeeForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
          <Route path="presentations/new" element={<PresentationForm />} />
        </Routes>
      </>
    </Router>
  );
}

export default App;
