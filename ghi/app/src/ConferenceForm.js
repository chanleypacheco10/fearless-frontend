import React, {useEffect, useState} from 'react';


function ConferenceForm () {
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = descriptions;
        data.max_presentations= maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setMaxPresentation('');
            setMaxAttendees('');
            setLocation('');
        }
    }


    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const [maxPresentations, setMaxPresentation] = useState('');
    const handleMaxPresentationChange = (event) => {
        const value = event.target.value;
        setMaxPresentation(value);
    }

    const [descriptions, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [ends, setEnd] = useState('');
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const [starts, setStart] = useState('');
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }




    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch(url);
        
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }
    
      useEffect(() => {
        fetchData();
      }, []);


    return (
      <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input value = {name} onChange={handleNameChange} placeholder="name" required type="text" id="name" name="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={starts} onChange={handleStartChange} placeholder="starts" required type="date" id="starts" name="starts" className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input value={ends} onChange={handleEndChange} placeholder="ends" required type="date" id="ends" name="ends" className="form-control" />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea value={descriptions} onChange={handleDescriptionChange} placeholder="description" required type="textarea" id="description" name="description" className="form-control"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input value={maxPresentations} onChange={handleMaxPresentationChange} placeholder="maximum presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="maximum attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default ConferenceForm;